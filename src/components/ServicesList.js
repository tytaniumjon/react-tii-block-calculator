import React, { Component } from 'react';
import ServiceBlock from './ServiceBlock';

export default class extends Component{
	
	constructor(props) {
		super(props);
		this.state = { services: props.services };
		console.log(this.state.services);
	}
	
	render() {
		console.log(this.props.services);
		const services = this.props.services;

		const list = services.map(
			service => <ServiceBlock key={service.id} service={service} />
		);

		return (
			<div className="services-list">
				<h1>{this.props.title}</h1>
				{ list }
				{/* {services.map(
					service => <ServiceBlock key={service.id} service={service} />
				)}  */}
			</div>
		)			
	};
}