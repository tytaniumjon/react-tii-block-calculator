import React from 'react';
// import "../styles/serviceblock.scss";

export default function ServiceBlock(props) {
	// const updateQty = (e) => {
	// 	console.log(e);
	// 	if (e.target === '<div class="qty-add"') {
	// 		console.log('You Clicked the Plus')
	// 		// this.props.qty++
	// 	}
	// };

	function addQty(qty){
		qty++;
	}

	function subQty(qty) {
		qty--;
	}
	
	return (
		<div key={props.id} className="block-service" data-type={props.serviceType}>
			<div className='block-service__title'>{props.title}</div>
			<div className="block-cost">{props.cost}</div>
			<div className="block-qty">
				<button className="qty-add qty-btn" onClick={addQty(props.qty)}>+</button>
				<div className="block-qty-value">{props.qty}</div>
				<button className="qty-sub qty-btn" onClick={subQty(props.qty)}>-</button>
			</div>
		</div>	
	);

}