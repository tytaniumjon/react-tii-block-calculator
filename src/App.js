import React, { Component } from 'react';
// import ServicesList from './components/ServicesList';
import './App.css';
import ServiceBlock from './Components/ServiceBlock';
// let serviceTitle = 'Choose your blocks';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      services: [],
    };
  }  

  getServicesData() {
    fetch('https://2018.dev.tytaniumideas.com/wp-json/wp/v2/block_service?per_page=100')
      .then(
        response => response.json())
      .then(
        servicesData => {
          this.setState({
            isLoaded: true,
            services: servicesData
          })
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  componentDidMount() {
    this.getServicesData();  
  }


  render() {
    const { error, isLoaded, services } = this.state;

      if (error) {
        return  <div className="App"><div>Error: {error.message}</div></div>
      } else if (!isLoaded) {
        return <div className="App"><h3>Loading...</h3></div>
      } else {
        return (
          <div className="App">
            <div className="services-list">
            {services.map( service => (
              <ServiceBlock key={service.id} id={service.id}  title={ service.title.rendered } serviceType={service.service_type} cost={service.block_cost} qty={0} />
              ))}    
            </div>
          </div>            
        );
      }
  }
}

export default App;
